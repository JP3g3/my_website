import React from 'react'
import { Link } from 'react-router-dom'
import './Transition.scss'

const Transition = ({ path, direction }) => {
  if (!path || !direction) return null
  return (
    <Link className='transitionLink' to={`/${path}`}>
      {direction === 'up' ? <div className="arrow up" /> : <div className="arrow down" />}
    </Link>
  )
}
export default Transition