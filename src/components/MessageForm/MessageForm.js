import React, { useState } from 'react'
import sentRequest from '../../utils/api'
import './MessageForm.scss'

const MessageForm = () => {
  const [name, setName] = useState('')
  const [message, setMessage] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const [buttonText, setButtonText] = useState('SUBMIT')

  const validateData = () => {
    if (name && message) {
      setIsLoading(true)
      setButtonText('LOADING')
      sendMail()
    }
  }

  const sendMail = () => {
    sentRequest(`Message from - ${name}`, message)
      .then((res) => {
        res.status === 200 ? setButtonText('MESSAGE SENT') : setButtonText('SENDING FAILED')
      })
      .catch((err) => {
        setButtonText('SENDING FAILED')
      })
      .then(() => setIsLoading(false))
  }

  return (
    <div className='messageForm'>
      <div>
        <input value={name} id='name' className='input' type='text' minLength='2' maxLength='60' onChange={val => setName(val.currentTarget.value)} required />
        <label htmlFor='name'>Full name</label>
      </div>
      <div>
        <textarea value={message} id='message' className='input' minLength='5' maxLength='2000' onChange={val => setMessage(val.currentTarget.value)} required />
        <label htmlFor='message'>Message</label>
      </div>
      <button className={isLoading ? 'submit loading' : 'submit'} type='submit' onClick={validateData}>
        {buttonText}
      </button>
    </div>
  )
}

export default MessageForm