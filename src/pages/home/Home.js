import React, { useState, useEffect } from "react"
import Logo from "../../components/Logo/Logo"
import "./Home.scss"

const Home = () => {
  const subTitles = ['web developer', 'frondend developer', 'ios developer', 'android developer']
  const [currentSubTitle, setCurrentSubTitle] = useState(subTitles[1])
  const [subTitleVisibility, setSubTitleVisibility] = useState(false)
  const lastAnimLetterIndex = 9

  useEffect(() => {
    const interval = setInterval(() => {
      const index = Math.floor(Math.random() * subTitles.length)
      setCurrentSubTitle(subTitles[index])
    }, 1500);

    if (!subTitleVisibility) {
      const paths = document.querySelectorAll('#logo path');
      if (paths.length > lastAnimLetterIndex) {
        paths[lastAnimLetterIndex].addEventListener('animationend', () => {
          setSubTitleVisibility(true)
        })
      }
    }

    return () => {
      clearInterval(interval);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className='homePage'>
      <div className='title'>
        <Logo />
        <h2 id='subTitle' style={subTitleVisibility ? { visibility: 'visible' } : { visibility: 'hidden' }}>
          {currentSubTitle}
        </h2>
      </div>
    </div>
  )
}

export default Home