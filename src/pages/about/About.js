import React from 'react'
import programmer from '../../assets/img/programmer.png'
import './About.scss'

const About = () => {
  const getAge = () => {
    const date = new Date()
    return date.getFullYear() - 1996
  }

  return (
    <div className='aboutPage'>
      <header><h1>About</h1></header>
      <main>
        <div className='clouds'>
          <div className='main'>Hi, I am a {getAge()}-year IT graduate who, with full willingness and enthusiasm, would accept the opportunity to develop as a frontend developer.</div>
          <div className='second'/>
          <div className='first' />
        </div>
        <img src={programmer} alt='programmer' width='250px' />
      </main>
    </div>
  )
}

export default About
