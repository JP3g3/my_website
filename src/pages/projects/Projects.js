import React from 'react'
import websiteImg from '../../assets/img/website.png'
import weatherImg from '../../assets/img/phones.png'
import editorImg from '../../assets/img/textEditor.png'
import './Projects.scss'

const Projects = () => {
    return (
        <div className='projectsPage'>
            <header><h1>Projects</h1></header>
            <main className='table'>
                <section className='element'>
                    <h3>
                        <a href='https://bitbucket.org/JP3g3/my_website/' rel='noopener noreferrer' target='_blank'>This website</a>
                    </h3>
                    <span>Technology: React, HTML5, SASS</span>
                    <img src={websiteImg} alt='this website' />
                </section>
                <section className='element'>
                    <h3>
                        <a href='https://bitbucket.org/JP3g3/weatherApp/' rel='noopener noreferrer' target='_blank'>Weather application</a>
                    </h3>
                    <span>Technology: Ionic, React, Typescript, HTML5, CSS3</span>
                    <img src={weatherImg} alt='weather application' />
                </section>
                <section className='element'>
                    <h3>
                        <a href='https://bitbucket.org/JP3g3/semanticTextEditor/' rel='noopener noreferrer' target='_blank'>Semantic text editor</a>
                    </h3>
                    <span>Technology: JS, HTML5, CSS3</span>
                    <img src={editorImg} alt='Semantic text editor' />
                </section>
                <section className='element'>
                    <h3>
                        <a href='https://bitbucket.org/jp3g3/' rel='noopener noreferrer' target='_blank'>Rest of projects</a>
                    </h3>
                </section>
            </main>
        </div>
    )
}

export default Projects