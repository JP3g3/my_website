import React from 'react'
import './Contact.scss'
import MessageForm from '../../components/MessageForm/MessageForm'

const Contact = () => {
    return (
        <div className='contactPage'>
            <header><h1>Contact</h1></header>
            <main>
                <p><strong>Let's do something together?</strong><br /> Get in touch for questions and partnerships at <a href='mailto:bartek.grab@yahoo.com'>bartek.grab@yahoo.com</a> or leave your message here! </p>
                <MessageForm />
            </main>
        </div>
    )
}

export default Contact