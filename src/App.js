import React, { useState, useEffect } from 'react'
import { BrowserRouter as Router, Redirect, Route, Link } from 'react-router-dom'
import Home from './pages/home/Home'
import About from './pages/about/About'
import Projects from './pages/projects/Projects'
import Contact from './pages/contact/Contact'
import homeIcon from './assets/icons/home.svg'
import personIcon from './assets/icons/person.svg'
import projectsIcon from './assets/icons/projects.svg'
import contactIcon from './assets/icons/contact.svg'
import './App.scss'

const App = () => {
  const [selectedPage, setSelectedPage] = useState('')

  useEffect(() => {
    const componentName = window.location.pathname.split("/").pop()
    setSelectedPage(componentName)
  }, [])

  const clickHandler = ({ currentTarget }) => {
    if (currentTarget.id) {
      setSelectedPage(currentTarget.id)
    }
  }

  return (
    <Router>
      <nav className="navigation">
        <Link id='home' className={selectedPage === 'home' ? 'item dim' : 'item'} to='/home' onClick={clickHandler}>
          <img src={homeIcon} alt='home' />
          <span className='name'>Home</span>
        </Link>
        <Link id='about' className={selectedPage === 'about' ? 'item dim' : 'item'} to='/about' onClick={clickHandler}>
          <img src={personIcon} alt='about' />
          <span className='name'>About</span>
        </Link>
        <Link id='projects' className={selectedPage === 'projects' ? 'item dim' : 'item'} to='/projects' onClick={clickHandler}>
          <img src={projectsIcon} alt='projects' />
          <span className='name'>Projects</span>
        </Link>
        <Link id='contact' className={selectedPage === 'contact' ? 'item dim' : 'item'} to='/contact' onClick={clickHandler}>
          <img src={contactIcon} alt='contact' />
          <span className='name'>Contact</span>
        </Link>
      </nav>

      <Route path='/home' component={Home} />
      <Route path='/about' component={About} />
      <Route path='/projects' component={Projects} />
      <Route path='/contact' component={Contact} />
      <Route exact path='/' render={() => <Redirect to="/home" />} />
    </Router>
  )
}

export default App
