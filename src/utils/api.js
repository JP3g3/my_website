const url = 'https://bartekgrabarzapi.netlify.app/.netlify/functions/server'
const header = {
  'Content-Type': 'application/json'
}

const sendRequest = (subject, text) => {
  return fetch(`${url}/mail`, {
    method: "POST",
    headers: header,
    body: JSON.stringify({
      mail: 'bartek.grab@yahoo.com',
      subject,
      text
    })
  })
}

export default sendRequest